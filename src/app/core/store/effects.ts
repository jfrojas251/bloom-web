import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { NgxNotificationMsgService, NgxNotificationDirection } from 'ngx-notification-msg';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

import * as actions from './actions';

@Injectable()
export class CoreEffects {

  constructor(
    private actions$: Actions,
    private notificationService: NgxNotificationMsgService,
    private router: Router
  ) {}

  notificationAction$ = createEffect(() => this.actions$.pipe(
    ofType(actions.notificationAction),
    tap(({ message, status }) => this.notificationService.open({ msg: message, status, direction: NgxNotificationDirection.BOTTOM_RIGHT }))
  ), { dispatch: false });

  navigateToAction$ = createEffect(() => this.actions$.pipe(
    ofType(actions.navigateToAction),
    tap(({ commands, extras }) => this.router.navigate(commands, extras))
  ), { dispatch: false });

}
