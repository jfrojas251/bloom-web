import { createAction, props } from '@ngrx/store';
import { NgxNotificationStatusMsg } from 'ngx-notification-msg';
import { NavigationExtras } from '@angular/router';

export const notificationAction = createAction(
  '[Core] Notification', props<{ message: string, status: NgxNotificationStatusMsg }>()
);
export const navigateToAction = createAction(
  '[Core] Navigate to', props<{ commands: any[], extras?: NavigationExtras }>()
);
