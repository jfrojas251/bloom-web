import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  public form: FormGroup;

  constructor() { }

  ngOnInit(): void {
    const { required, email } = Validators;
    this.form = new FormGroup({
      email: new FormControl('', [required, email]),
      password: new FormControl('', [required])
    });
  }

}
