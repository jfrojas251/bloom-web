import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { AuthFacade } from '../../auth-facade';
import { SignupStep } from '../../auth.constants';
import { User } from '../../auth.entities';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.sass']
})
export class SignupComponent implements OnInit {

  public stepOneForm: FormGroup;
  public stepFourForm: FormGroup;
  public stepFiveForm: FormGroup;
  public stepSixForm: FormGroup;
  public stepSevenForm: FormGroup;
  public steps: typeof SignupStep = SignupStep;

  constructor(
    private authFacade: AuthFacade,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    // TODO: Validacione fuertes de campos registro

    this.route.queryParams.pipe(
      filter(params => !!params.e && !!params.e)
    ).subscribe(params => {
      this.authFacade.emailVerifyStep(params.e, params.t);
    }).unsubscribe();

    const { required, email } = Validators;
    this.stepOneForm = new FormGroup({
      names: new FormControl('Julian Rojas', [required]),
      emails: new FormGroup({
        email: new FormControl('julian@bloomcrowdfunding.co', [required, email]),
        confirmationEmail: new FormControl('julian@bloomcrowdfunding.co', [required, email])
      }, { validators: [this.validateConfirmEmail] }),
    });
    this.stepFourForm = new FormGroup({
      phone: new FormControl('', [required])
    });
    this.stepFiveForm = new FormGroup({
      code: new FormControl('', [required])
    });
    this.stepSixForm = new FormGroup({
      documentType: new FormControl(1, [required]),
      document: new FormControl('', [required]),
      expeditionDate: new FormControl('2000-05-01', [required]),
    });
    this.stepSevenForm = new FormGroup({
      password: new FormControl('', [required]),
      repeatPassword: new FormControl('', [required]),
    });
  }

  get currentStep$(): Observable<SignupStep> {
    return this.authFacade.currentSignupStep$;
  }

  get user$(): Observable<User> {
    return this.authFacade.user$;
  }

  private validateConfirmEmail = (c: AbstractControl): ValidationErrors => {
    return c.get('email').value === c.get('confirmationEmail').value
      ? null : { validateConfirmPassword: { valid: false } };
  }

  public close(): void {
    if (this.router.url.includes('signup/verify')) {
      this.router.navigate(['/']);
    } else {
      this.router.navigate(['../../', { outlets: { auth: null } }], {
        skipLocationChange: true
      });
    }
  }

  public onSubmitStepOne(): void {
    const { names, emails } = this.stepOneForm.value;
    this.authFacade.startStep(names, emails.email);
  }

  public onSubmitStepFour(): void {
    this.user$.subscribe(user => {
      const { phone } = this.stepFourForm.value;
      this.authFacade.phoneVerifyStep(user.id, phone);
    }).unsubscribe();
  }

  public onSubmitStepFive(): void {
    this.user$.subscribe(user => {
      const { phone } = this.stepFourForm.value;
      const { code } = this.stepFiveForm.value;
      this.authFacade.phoneValidateStep(user.id, phone, code);
    }).unsubscribe();
  }

  public onSubmitStepSix(): void {
    this.user$.subscribe(user => {
      const { documentType, document, expeditionDate } = this.stepSixForm.value;
      this.authFacade.documentVerifyStep(user.id, documentType, document, expeditionDate);
    }).unsubscribe();
  }

  public onSubmitStepSeven(): void {
    // TODO: Hacer logica del MAC o ID de navegador
    const mac = '123123';
    this.user$.subscribe(user => {
      const { password, repeatPassword } = this.stepSevenForm.value;
      this.authFacade.passwordVerifyStep(user.id, password, repeatPassword, mac);
    }).unsubscribe();
  }

}
