export interface User {
  countryId: number;
  idTypeDocument: number;
  state: number;
  email: string;
  id: string;
  nameState: string;
  username: string;
  phone?: string;
  phoneLast?: string;
}
