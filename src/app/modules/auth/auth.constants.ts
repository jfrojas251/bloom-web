export enum SignupStep {
  Start = 1,
  EmailVerify = 2,
  IsEmailVerified = 3,
  PhoneVerify = 4,
  ValidatePhone = 5,
  DocumentVerify = 6,
  PasswordVerify = 7,
  Active = 8
}

export enum UserState {

}
