import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { RouterModule } from '@angular/router';

import { authFeatureName } from './store/state';
import { authReducers } from './store/reducers';
import { AuthEffects } from './store/effects';
import { AuthFacade } from './auth-facade';
import { AuthServices } from './auth.services';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';

@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    SignupComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    StoreModule.forFeature(authFeatureName, authReducers),
    EffectsModule.forFeature([ AuthEffects ]),
    RouterModule,
  ],
  providers: [
    AuthFacade,
    AuthEffects,
    AuthServices,
  ]
})
export class AuthModule { }
