import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { SignupStep } from './auth.constants';
import { User } from './auth.entities';
import * as selectors from './store/selectors';
import * as actions from './store/actions';

@Injectable()
export class AuthFacade {

  constructor(
    private store: Store
  ) {}

  public user$: Observable<User> = this.store.pipe(
    select(selectors.userSelector)
  );

  public currentSignupStep$: Observable<SignupStep> = this.store.pipe(
    select(selectors.stepSelector)
  );

  public startStep(names: string, email: string): void {
    this.store.dispatch(actions.registerStepOneAction({ names, email }));
  }

  public emailVerifyStep(email: string, token: string): void {
    this.store.dispatch(actions.registerStepTwoAction({ email, token }));
  }

  public phoneVerifyStep(userId: string, phone: string): void {
    this.store.dispatch(actions.registerStepFourAction({ userId, phone }));
  }

  public phoneValidateStep(userId: string, phone: string, code: string): void {
    this.store.dispatch(actions.registerStepFiveAction({ userId, phone, code }));
  }

  public documentVerifyStep(userId: string, documentType: number, document: string, expeditionDate: string): void {
    this.store.dispatch(actions.registerStepSixAction({ userId, documentType, document, expeditionDate }));
  }

  public passwordVerifyStep(userId: string, password: string, repeatPassword: string, mac: string): void {
    this.store.dispatch(actions.registerStepSevenAction({ userId, password, repeatPassword, mac }));
  }
}
