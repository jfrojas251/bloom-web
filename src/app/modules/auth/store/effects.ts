import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { switchMap, catchError, map, concatMap } from 'rxjs/operators';
import { NgxNotificationStatusMsg } from 'ngx-notification-msg';
import { of } from 'rxjs';

import * as actions from './actions';
import { AuthServices } from '../auth.services';
import { SignupStep } from '../auth.constants';
import { notificationAction, navigateToAction } from '../../../core/store/actions';

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private services: AuthServices,
  ) {}

  registerStatusAction$ = createEffect(() => this.actions$.pipe(
    ofType(actions.registerStatusAction),
    switchMap(({ email }) => this.services.registerStatus$(email).pipe(
      map(response => ({ response, error: null })),
      catchError(error => of({ error, response: null }))
    )),
    concatMap(({ response, error }) => {
      if (error) {
        return [
          actions.registerStatusErrorAction(),
          notificationAction({ message: 'Error!', status: NgxNotificationStatusMsg.FAILURE })
        ];
      }
      if (response.status) {
        let step = SignupStep.Start;
        if (response.data.nameState === 'REGISTER_EMAIL_VERIFIED') { step = SignupStep.PhoneVerify; }
        if (response.data.nameState === 'REGISTER_PHONE_VERIFIED') { step = SignupStep.DocumentVerify; }
        if (response.data.nameState === 'REGISTER_DOCUMENT_VERIFIED') { step = SignupStep.PasswordVerify; }
        if (response.data.nameState === 'ACTIVE') {
          return [navigateToAction({ commands: [{ outlets: { auth: ['auth', 'login'] } }], extras: { skipLocationChange: true} })];
        }
        return [
          actions.registerStatusSuccessAction({ user: response.data }),
          actions.setCurrentSignupStepAction({ step })
        ];
      } else {
        return [
          actions.registerStepTwoErrorAction(),
          notificationAction({ message: response.data, status: NgxNotificationStatusMsg.FAILURE })
        ];
      }
    })
  ));

  registerStepOneAction$ = createEffect(() => this.actions$.pipe(
    ofType(actions.registerStepOneAction),
    switchMap(({ names, email }) => this.services.registerStepOne$(names, email).pipe(
      map(response => ({ response, email, error: null })),
      catchError(error => of({ error, email, response: null }))
    )),
    concatMap(({ response, email, error }) => {
      if (error) {
        return [
          actions.registerStepOneErrorAction(),
          notificationAction({ message: 'Error!', status: NgxNotificationStatusMsg.FAILURE })
        ];
      }
      if (response.status) {
        return [
          actions.registerStepOneSuccessAction(),
          notificationAction({ message: 'Email enviado', status: NgxNotificationStatusMsg.SUCCESS }),
          actions.setCurrentSignupStepAction({ step: SignupStep.EmailVerify })
        ];
      } else {
        if (response.message === 'EMAIL_EXISTS') {
          return [
            actions.registerStatusAction({ email })
          ];
        }
        return [
          actions.registerStepOneErrorAction(),
          notificationAction({ message: response.data, status: NgxNotificationStatusMsg.FAILURE })
        ];
      }
    })
  ));

  registerStepTwoAction$ = createEffect(() => this.actions$.pipe(
    ofType(actions.registerStepTwoAction),
    switchMap(({ email, token }) => this.services.registerStepTwo$(email, token).pipe(
      map(response => ({ response, error: null })),
      catchError(error => of({ error, response: null }))
    )),
    concatMap(({ response, error }) => {
      if (error) {
        return [
          actions.registerStepTwoErrorAction(),
          notificationAction({ message: 'Error!', status: NgxNotificationStatusMsg.FAILURE })
        ];
      }
      return response.status ? [
        actions.registerStepTwoSuccessAction({ user: response.data }),
        notificationAction({ message: 'Email verificado', status: NgxNotificationStatusMsg.SUCCESS }),
        actions.setCurrentSignupStepAction({ step: SignupStep.PhoneVerify })
      ] : [
        actions.registerStepTwoErrorAction(),
        notificationAction({ message: response.data, status: NgxNotificationStatusMsg.FAILURE })
      ];
    })
  ));

  registerStepFourAction$ = createEffect(() => this.actions$.pipe(
    ofType(actions.registerStepFourAction),
    switchMap(({ userId, phone }) => this.services.registerStepFour$(userId, phone).pipe(
      map(response => ({ response, error: null })),
      catchError(error => of({ error, response: null }))
    )),
    concatMap(({ response, error }) => {
      if (error) {
        return [
          actions.registerStepFourErrorAction(),
          notificationAction({ message: 'Error!', status: NgxNotificationStatusMsg.FAILURE })
        ];
      }
      return response.status ? [
        actions.registerStepFourSuccessAction(),
        notificationAction({ message: 'Se ha enviado un mensaje a tu telefono', status: NgxNotificationStatusMsg.SUCCESS }),
        actions.setCurrentSignupStepAction({ step: SignupStep.ValidatePhone })
      ] : [
        actions.registerStepFourErrorAction(),
        notificationAction({ message: response.data, status: NgxNotificationStatusMsg.FAILURE })
      ];
    })
  ));

  registerStepFiveAction$ = createEffect(() => this.actions$.pipe(
    ofType(actions.registerStepFiveAction),
    switchMap(({ userId, phone, code }) => this.services.registerStepFive$(userId, phone, code).pipe(
      map(response => ({ response, error: null })),
      catchError(error => of({ error, response: null }))
    )),
    concatMap(({ response, error }) => {
      if (error) {
        return [
          actions.registerStepFiveErrorAction(),
          notificationAction({ message: 'Error!', status: NgxNotificationStatusMsg.FAILURE })
        ];
      }
      return response.status ? [
        actions.registerStepFiveSuccessAction({ user: response.data }),
        notificationAction({ message: 'Se ha validado correctamente tu telefono', status: NgxNotificationStatusMsg.SUCCESS }),
        actions.setCurrentSignupStepAction({ step: SignupStep.DocumentVerify })
      ] : [
        actions.registerStepFiveErrorAction(),
        notificationAction({ message: response.data, status: NgxNotificationStatusMsg.FAILURE })
      ];
    })
  ));

  registerStepSixAction$ = createEffect(() => this.actions$.pipe(
    ofType(actions.registerStepSixAction),
    switchMap(({ document, documentType, expeditionDate, userId }) =>
      this.services.registerStepSix$(userId, documentType, document, expeditionDate).pipe(
        map(response => ({ response, error: null })),
        catchError(error => of({ error, response: null }))
      )
    ),
    concatMap(({ response, error }) => {
      if (error) {
        return [
          actions.registerStepSixErrorAction(),
          notificationAction({ message: 'Error!', status: NgxNotificationStatusMsg.FAILURE })
        ];
      }
      return response.status ? [
        actions.registerStepSixSuccessAction({ user: response.data }),
        notificationAction({ message: 'Se ha validado correctamente tu documento', status: NgxNotificationStatusMsg.SUCCESS }),
        actions.setCurrentSignupStepAction({ step: SignupStep.PasswordVerify })
      ] : [
        actions.registerStepSixErrorAction(),
        notificationAction({ message: response.data, status: NgxNotificationStatusMsg.FAILURE })
      ];
    })
  ));

  registerStepSevenAction$ = createEffect(() => this.actions$.pipe(
    ofType(actions.registerStepSevenAction),
    switchMap(({ userId, password, repeatPassword, mac }) =>
      this.services.registerStepSeven$(userId, password, repeatPassword, mac).pipe(
        map(response => ({ response, error: null })),
        catchError(error => of({ error, response: null }))
      )
    ),
    concatMap(({ response, error }) => {
      if (error) {
        return [
          actions.registerStepSevenErrorAction(),
          notificationAction({ message: 'Error!', status: NgxNotificationStatusMsg.FAILURE })
        ];
      }
      return response.status ? [
        actions.registerStepSevenSuccessAction({ user: response.data }),
        notificationAction({ message: 'Se ha creado tu contraseña correctamnete.', status: NgxNotificationStatusMsg.SUCCESS }),
        actions.setCurrentSignupStepAction({ step: SignupStep.PasswordVerify })
      ] : [
        actions.registerStepSevenErrorAction(),
        notificationAction({ message: response.data, status: NgxNotificationStatusMsg.FAILURE })
      ];
    })
  ));
}
