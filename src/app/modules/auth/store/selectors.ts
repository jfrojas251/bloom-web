import { createSelector, createFeatureSelector } from '@ngrx/store';
import { authFeatureName, AuthState } from './state';

const authStateSelector = createFeatureSelector<AuthState>(authFeatureName);

export const uiSelector = createSelector(authStateSelector, state => state.ui);
export const stepSelector = createSelector(authStateSelector, state => state.data.currentSignupStep);
export const userSelector = createSelector(authStateSelector, state => state.data.user);
