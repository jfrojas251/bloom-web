import { SignupStep } from '../auth.constants';
import { User } from '../auth.entities';

export const authFeatureName = 'auth';

export interface AuthState {
  ui: any;
  data: AuthDataState;
}

export interface AuthDataState {
  currentSignupStep: SignupStep;
  user: User;
}
