import { createReducer, on, combineReducers } from '@ngrx/store';

import { AuthDataState } from './state';
import { SignupStep } from '../auth.constants';
import * as actions from './actions';

const initialDataState: AuthDataState = {
  currentSignupStep: SignupStep.Start,
  user: null
};

const data = createReducer(
  initialDataState,
  on(actions.setCurrentSignupStepAction, (state, { step }) => ({ ...state, currentSignupStep: step })),
  on(actions.registerStatusSuccessAction, (state, { user }) => ({ ...state, user })),
  on(actions.registerStepTwoSuccessAction, (state, { user }) => ({ ...state, user })),
  on(actions.registerStepFiveSuccessAction, (state, { user }) => ({ ...state, user })),
  on(actions.registerStepSixSuccessAction, (state, { user }) => ({ ...state, user })),
);

const initialUiState: any = {

};

const ui = createReducer(
  initialUiState,
  // on(null, (state, payload) => state)
);

export const authReducers = combineReducers({
  ui,
  data
});
