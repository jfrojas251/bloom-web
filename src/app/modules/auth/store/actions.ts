import { createAction, props } from '@ngrx/store';

import { SignupStep } from '../auth.constants';
import { User } from '../auth.entities';

export const setCurrentSignupStepAction = createAction(
  '[Auth] Set current signup step', props<{ step: SignupStep }>()
);
export const registerStatusAction = createAction(
  '[Auth] Register status', props<{ email: string }>()
);
export const registerStatusSuccessAction = createAction(
  '[Auth] Register status success', props<{ user: User }>()
);
export const registerStatusErrorAction = createAction(
  '[Auth] Register status error'
);
export const registerStepOneAction = createAction(
  '[Auth] Register step one', props<{ names: string, email: string }>()
);
export const registerStepOneSuccessAction = createAction(
  '[Auth] Register step one success'
);
export const registerStepOneErrorAction = createAction(
  '[Auth] Register step one error'
);
export const registerStepTwoAction = createAction(
  '[Auth] Register step two', props<{ email: string, token: string }>()
);
export const registerStepTwoSuccessAction = createAction(
  '[Auth] Register step two success', props<{ user: User }>()
);
export const registerStepTwoErrorAction = createAction(
  '[Auth] Register step two error'
);
export const registerStepFourAction = createAction(
  '[Auth] Register step four', props<{ userId: string, phone: string }>()
);
export const registerStepFourSuccessAction = createAction(
  '[Auth] Register step four success'
);
export const registerStepFourErrorAction = createAction(
  '[Auth] Register step four error'
);
export const registerStepFiveAction = createAction(
  '[Auth] Register step five', props<{ userId: string, phone: string, code: string }>()
);
export const registerStepFiveSuccessAction = createAction(
  '[Auth] Register step five success', props<{ user: User }>()
);
export const registerStepFiveErrorAction = createAction(
  '[Auth] Register step five error'
);
export const registerStepSixAction = createAction(
  '[Auth] Register step six', props<{ userId: string, documentType: number, document: string, expeditionDate: string }>()
);
export const registerStepSixSuccessAction = createAction(
  '[Auth] Register step six success', props<{ user: User }>()
);
export const registerStepSixErrorAction = createAction(
  '[Auth] Register step six error'
);
export const registerStepSevenAction = createAction(
  '[Auth] Register step seven', props<{ userId: string, password: string, repeatPassword: string, mac: string }>()
);
export const registerStepSevenSuccessAction = createAction(
  '[Auth] Register step seven success', props<{ user: User }>()
);
export const registerStepSevenErrorAction = createAction(
  '[Auth] Register step seven error'
);
