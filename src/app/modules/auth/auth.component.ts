import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.sass']
})
export class AuthComponent {

  constructor(
    private router: Router
  ) { }

  public close(): void {
    if (this.router.url.includes('signup/verify')) {
      this.router.navigate(['/']);
    } else {
      this.router.navigate(['../../', { outlets: { auth: null } }], {
        skipLocationChange: true
      });
    }
  }

}
