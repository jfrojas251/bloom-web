import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { User } from './auth.entities';

const api = {
  status: (email: string) => `http://129.213.119.245:8280/user/register/register-status/${email}`,
  stepOne: () => `http://129.213.119.245:8280/user/register/start`,
  stepTwo: () => `http://129.213.119.245:8280/user/register/email-verify`,
  stepFour: () => `http://129.213.119.245:8280/user/register/phone-verify`,
  stepFive: () => `http://129.213.119.245:8280/user/register/validate-phone`,
  stepSix: () => `http://129.213.119.245:8280/user/register/document-verify`,
  stepSeven: () => `http://129.213.119.245:8280/user/register/password-verify`,
};

@Injectable()
export class AuthServices {
  constructor(
    private http: HttpClient
  ) {}

  public registerStatus$(email: string): Observable<{status: boolean, message: string, data: any}> {
    return this.http.get<{status: boolean, message: string, data: any}>(api.status(email));
  }

  public registerStepOne$(names: string, email: string): Observable<{status: boolean, message: string, data: any}> {
    return this.http.post<{status: boolean, message: string, data: any}>(api.stepOne(), { names, email });
  }

  public registerStepTwo$(email: string, token: string): Observable<{status: boolean, message: string, data: User}> {
    return this.http.post<{status: boolean, message: string, data: User}>(api.stepTwo(), { email, token });
  }

  public registerStepFour$(userId: string, phone: string): Observable<{status: boolean, message: string, data: User}> {
    return this.http.post<{status: boolean, message: string, data: User}>(api.stepFour(), { idUser: userId, phone });
  }

  public registerStepFive$(userId: string, phone: string, code: string): Observable<{status: boolean, message: string, data: User}> {
    return this.http.post<{status: boolean, message: string, data: User}>(api.stepFive(), { idUser: userId, phone, code });
  }

  public registerStepSix$(
    userId: string, documentType: number, document: string, expeditionDate: string
  ): Observable<{status: boolean, message: string, data: User}> {
    return this.http.post<{status: boolean, message: string, data: User}>(api.stepSix(), {
      idUser: userId,
      idTypeDocument: documentType,
      dni: document,
      dateDocument: expeditionDate
    });
  }

  public registerStepSeven$(
    userId: string, password: string, repeatPassword: string, mac: string
  ): Observable<{status: boolean, message: string, data: User}> {
    return this.http.post<{status: boolean, message: string, data: User}>(api.stepSeven(), {
      idUser: userId, password, repeatPassword, mac
    });
  }

}
