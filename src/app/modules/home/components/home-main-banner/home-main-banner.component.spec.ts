import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeMainBannerComponent } from './home-main-banner.component';

describe('HomeMainBannerComponent', () => {
  let component: HomeMainBannerComponent;
  let fixture: ComponentFixture<HomeMainBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeMainBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeMainBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
