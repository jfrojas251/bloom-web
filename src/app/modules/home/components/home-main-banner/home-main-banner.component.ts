import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-home-main-banner',
  templateUrl: './home-main-banner.component.html',
  styleUrls: ['./home-main-banner.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeMainBannerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
