import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeMainActionsBoxComponent } from './home-main-actions-box.component';

describe('HomeMainActionsBoxComponent', () => {
  let component: HomeMainActionsBoxComponent;
  let fixture: ComponentFixture<HomeMainActionsBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeMainActionsBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeMainActionsBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
