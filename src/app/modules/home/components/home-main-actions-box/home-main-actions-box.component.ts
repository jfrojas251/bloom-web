import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-home-main-actions-box',
  templateUrl: './home-main-actions-box.component.html',
  styleUrls: ['./home-main-actions-box.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeMainActionsBoxComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
