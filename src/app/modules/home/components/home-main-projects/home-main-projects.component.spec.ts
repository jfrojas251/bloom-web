import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeMainProjectsComponent } from './home-main-projects.component';

describe('HomeMainProjectsComponent', () => {
  let component: HomeMainProjectsComponent;
  let fixture: ComponentFixture<HomeMainProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeMainProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeMainProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
