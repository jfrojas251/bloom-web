import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-home-main-projects',
  templateUrl: './home-main-projects.component.html',
  styleUrls: ['./home-main-projects.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeMainProjectsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
