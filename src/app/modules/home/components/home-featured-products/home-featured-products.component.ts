import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-home-featured-products',
  templateUrl: './home-featured-products.component.html',
  styleUrls: ['./home-featured-products.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeFeaturedProductsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
