import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-home-main-project-card',
  templateUrl: './home-main-project-card.component.html',
  styleUrls: ['./home-main-project-card.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeMainProjectCardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
