import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeMainProjectCardComponent } from './home-main-project-card.component';

describe('HomeMainProjectCardComponent', () => {
  let component: HomeMainProjectCardComponent;
  let fixture: ComponentFixture<HomeMainProjectCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeMainProjectCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeMainProjectCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
