import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeSecondBannerComponent } from './home-second-banner.component';

describe('HomeSecondBannerComponent', () => {
  let component: HomeSecondBannerComponent;
  let fixture: ComponentFixture<HomeSecondBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeSecondBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeSecondBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
