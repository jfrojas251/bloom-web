import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-home-second-banner',
  templateUrl: './home-second-banner.component.html',
  styleUrls: ['./home-second-banner.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeSecondBannerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
