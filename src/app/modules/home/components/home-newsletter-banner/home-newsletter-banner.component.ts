import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-home-newsletter-banner',
  templateUrl: './home-newsletter-banner.component.html',
  styleUrls: ['./home-newsletter-banner.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeNewsletterBannerComponent implements OnInit {

  public form: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('')
    });
  }

}
