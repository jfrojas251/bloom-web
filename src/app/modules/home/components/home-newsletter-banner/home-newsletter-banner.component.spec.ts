import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeNewsletterBannerComponent } from './home-newsletter-banner.component';

describe('HomeNewsletterBannerComponent', () => {
  let component: HomeNewsletterBannerComponent;
  let fixture: ComponentFixture<HomeNewsletterBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeNewsletterBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeNewsletterBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
