import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeUpcomingReleasesComponent } from './home-upcoming-releases.component';

describe('HomeUpcomingReleasesComponent', () => {
  let component: HomeUpcomingReleasesComponent;
  let fixture: ComponentFixture<HomeUpcomingReleasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeUpcomingReleasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeUpcomingReleasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
