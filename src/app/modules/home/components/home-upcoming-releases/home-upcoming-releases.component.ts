import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-upcoming-releases',
  templateUrl: './home-upcoming-releases.component.html',
  styleUrls: ['./home-upcoming-releases.component.sass']
})
export class HomeUpcomingReleasesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
