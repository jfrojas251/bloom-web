import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { HomeComponent } from './home.component';
import { HomeMainBannerComponent } from './components/home-main-banner/home-main-banner.component';
import { HomeMainActionsBoxComponent } from './components/home-main-actions-box/home-main-actions-box.component';
import { HomeFeaturedProductsComponent } from './components/home-featured-products/home-featured-products.component';
import { HomeMainProjectsComponent } from './components/home-main-projects/home-main-projects.component';
import { HomeMainProjectCardComponent } from './components/home-main-project-card/home-main-project-card.component';
import { HomeNewsletterBannerComponent } from './components/home-newsletter-banner/home-newsletter-banner.component';
import { HomeSecondBannerComponent } from './components/home-second-banner/home-second-banner.component';
import { HomeUpcomingReleasesComponent } from './components/home-upcoming-releases/home-upcoming-releases.component';

@NgModule({
  declarations: [
    HomeComponent, 
    HomeMainBannerComponent, 
    HomeMainActionsBoxComponent, 
    HomeFeaturedProductsComponent, 
    HomeMainProjectsComponent, 
    HomeMainProjectCardComponent, 
    HomeNewsletterBannerComponent, HomeSecondBannerComponent, HomeUpcomingReleasesComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class HomeModule { }
