import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgxNotificationMsgModule } from 'ngx-notification-msg';

import { environment } from '@environment';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderModule } from './modules/header/header.module';
import { AuthModule } from './modules/auth/auth.module';
import { CoreEffects } from './core/store/effects';
import { EncodeHttpParamsInterceptor } from './core/interceptors/encode-http-params-interceptor';
import { HomeModule } from './modules/home/home.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxNotificationMsgModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([
      CoreEffects
    ]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    HeaderModule,
    AuthModule,
    HomeModule,
    AppRoutingModule,
  ],
  providers: [
    CoreEffects,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: EncodeHttpParamsInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
